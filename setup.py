from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(name='opendota2py',
      version='1.0',
      description='OpenDota API Interface',
      url='https://gitlab.com/avalonparton/opendota2py',
      long_description=long_description,
      long_description_content_type="text/markdown",
      author='Avalon Parton',
      author_email='avalonlee@gmail.com',
      license='MIT',
      packages=['opendota2py'],
      install_requires=['requests'],
      zip_safe=False)