py -3 -m pip install twine wheel
py -3 setup.py sdist bdist_wheel
py -3 -m twine upload --verbose dist/*

# cleanup
rm -r dist