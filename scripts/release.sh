#!/bin/bash

python3 -m pip install twine wheel
python3 setup.py sdist bdist_wheel
python3 -m twine upload --verbose dist/*