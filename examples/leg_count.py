import sys
sys.path.append('..')
sys.path.append('.')
from opendota2py import Hero, Player


def avg_leg_count(player, days=30):
    """ Returns average leg count for a Player """
    matches = player.matches(date=days)
    print(f"  - Found {len(matches)} matches over {days} days")

    # Create a list where each item is the leg count of a match played
    legs = []
    for match in matches:
        if match.hero_id:
            hero = Hero(match.hero_id)
            legs.append(hero.legs)

    if legs:
        return sum(legs) / len(legs)
    else:
        return 0

for acc_id in [82279028, 108644835, 63853422, 81275416, 57974352]:
    player = Player(acc_id)
    print(f"Loading average leg count for {player.personaname} - {player.mmr_estimate} mmr")
    avg = avg_leg_count(player)
    print(f"  - Average legs: {avg:.4}")
    if player.mmr_estimate and avg > 0:
        mmr_per_leg = player.mmr_estimate / avg
        print(f"  - MMR per leg: {mmr_per_leg:.3f}")
