""" Tests Match functionality
https://docs.opendota.com/#tag/matches
"""
import sys
sys.path.append(".")
sys.path.append("..")
from opendota2py import Hero, Match, Player


def test_match():
    match = Match(3423126303)

    # Check basic attributes
    assert isinstance(match, Match)
    assert isinstance(match.info, str)
    assert isinstance(match.cluster, int)
    assert isinstance(match.cosmetics, dict)
    assert isinstance(match.dire_score, int)
    assert isinstance(match.dire_team, (int, type(None)))
    assert isinstance(match.draft_timings, (list, type(None)))
    assert isinstance(match.duration, int)
    assert isinstance(match.engine, int)
    assert isinstance(match.first_blood_time, int)
    assert isinstance(match.game_mode, int)
    assert isinstance(match.human_players, int)
    assert isinstance(match.id, int)
    assert isinstance(match.league_id, (int, type(None)))
    assert isinstance(match.lobby_type, int)
    assert isinstance(match.match_id, int)
    assert isinstance(match.match_seq_num, int)
    assert isinstance(match.negative_votes, int)
    assert isinstance(match.positive_votes, int)
    assert isinstance(match.patch, int)
    assert isinstance(match.players, list)
    assert isinstance(match.picks_bans, (list, type(None)))
    assert isinstance(match.radiant_gold_adv, list)
    assert isinstance(match.radiant_xp_adv, list)
    assert isinstance(match.radiant_score, int)
    assert isinstance(match.radiant_team, (int, type(None)))
    assert isinstance(match.radiant_win, bool)
    assert isinstance(match.region, int)
    assert isinstance(match.replay_salt, int)
    assert isinstance(match.series_id, int)
    assert isinstance(match.series_type, int)
    assert isinstance(match.skill, int)
    assert isinstance(match.start_time, int)
    assert isinstance(match.teamfights, list)
    assert isinstance(match.url, str)
    assert 'http' in match.url
    assert isinstance(match.replay_url, str)
    assert 'http' in match.replay_url
    assert isinstance(match.version, (int, type(None)))
    assert isinstance(match.league, type(None))
    assert match._get("missing_attribute") == None

    # Word counts
    assert isinstance(match.all_word_counts, dict)
    assert isinstance(match.my_word_counts, int)

    # Advantages
    assert isinstance(match.throw, (int, type(None)))
    assert isinstance(match.comeback, (int, type(None)))
    assert isinstance(match.loss, (int, type(None)))
    assert isinstance(match.win, (int, type(None)))

    # Bitmasks
    assert match.barracks_status_dire < 64
    assert match.barracks_status_radiant < 64
    assert match.tower_status_dire < 2048
    assert match.tower_status_radiant < 2048

    # Chat wheel {'time': 318, 'type': 'chatwheel', 'key': '90', 'slot': 2, 'player_slot': 2}
    assert match.chat[0].get("time") == 318
    assert match.chat[0].get("type") == 'chatwheel'
    assert match.chat[0].get("key") == '90'
    assert match.chat[0].get("slot") == 2
    assert match.chat[0].get("player_slot") == 2
    # Chat message {'time': 2013, 'type': 'chat', 'unit': '" El Oso Yogui ♥ "', 'key': 'GG', 'slot': 6, 'player_slot': 129}
    assert match.chat[12].get("time") == 2013
    assert match.chat[12].get("type") == 'chat'
    assert '♥' in match.chat[12].get("unit")
    assert match.chat[12].get("key") == 'GG'
    assert match.chat[12].get("slot") == 6
    assert match.chat[12].get("player_slot") == 129

    assert isinstance(match.objectives, list)



    # Check __str__()
    print("Done checking match:", match)