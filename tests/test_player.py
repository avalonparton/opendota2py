""" Tests Player functionality
https://docs.opendota.com/#tag/players
"""
import datetime
import sys

from opendota2py import Hero, Match, Player

sys.path.append(".")
sys.path.append("..")

ACCOUNT_ID = 82279028

def test_player():
    player = Player(ACCOUNT_ID)
    player.refresh()

    # Check basic attributes
    assert player.id == ACCOUNT_ID
    assert player.account_id == ACCOUNT_ID
    assert isinstance(player.info, str)
    assert isinstance(player.avatar, str)
    assert 'http' in player.avatar
    assert isinstance(player.avatarfull, str)
    assert 'http' in player.avatarfull
    assert isinstance(player.avatarmedium, str)
    assert 'http' in player.avatarmedium
    assert isinstance(player.url, str)
    assert 'http' in player.url
    assert isinstance(player.cheese, int)
    assert isinstance(player.competitive_rank, (int, type(None)))
    assert isinstance(player.is_contributor, bool)
    assert isinstance(player.last_login, datetime.datetime)
    assert isinstance(player.loccountrycode, str)
    assert isinstance(player.mmr_estimate, int)
    assert isinstance(player.name, (str, type(None)))
    assert isinstance(player.personaname, str)
    assert isinstance(player.profileurl, str)
    assert isinstance(player.rank_tier, (int, type(None)))
    assert isinstance(player.tracked_until, datetime.datetime)
    assert isinstance(player.steamid, str)
    assert player._get("missing_attribute") == None
    # Check __str__()
    print("Done checking player:", player)


def test_histograms():
    player = Player(ACCOUNT_ID)
    histograms = player.histograms(offset=1, field="kills")
    assert isinstance(histograms, list)
    
def test_wardmap():
    player = Player(ACCOUNT_ID)
    wardmap = player.wardmap(offset=1)
    assert isinstance(wardmap['obs'], dict)
    assert isinstance(wardmap['sen'], dict)
    
def test_wordcloud():
    player = Player(ACCOUNT_ID)
    wordcloud = player.wordcloud(offset=1)
    assert isinstance(wordcloud['my_word_counts'], dict)
    assert isinstance(wordcloud['all_word_counts'], dict)



def test_ratings():
    player = Player(ACCOUNT_ID)
    assert isinstance(player.ratings[0]['account_id'], int)
    assert isinstance(player.ratings[0]['match_id'], (int, type(None)))
    assert isinstance(player.ratings[0]['solo_competitive_rank'], int)
    assert isinstance(player.ratings[0]['competitive_rank'], (int, type(None)))
    assert isinstance(player.ratings[0]['time'], str)


def test_rankings():
    player = Player(ACCOUNT_ID)
    assert isinstance(player.rankings[0]['hero_id'], int)
    assert isinstance(player.rankings[0]['score'], float)
    assert isinstance(player.rankings[0]['percent_rank'], float)
    assert isinstance(player.rankings[0]['card'], int)


def test_recent_matches():
    player = Player(ACCOUNT_ID)
    assert isinstance(player.recent_matches[0], Match)
    assert isinstance(player.recent_matches[0].player_slot, int)
    assert isinstance(player.recent_matches[0].radiant_win, bool)
    assert isinstance(player.recent_matches[0].duration, int)
    assert isinstance(player.recent_matches[0].game_mode, int)
    assert isinstance(player.recent_matches[0].lobby_type, int)
    assert isinstance(player.recent_matches[0].hero_id, int)
    assert isinstance(player.recent_matches[0].start_time, int)
    assert isinstance(player.recent_matches[0].version, (int, type(None)))
    assert isinstance(player.recent_matches[0].kills, int)
    assert isinstance(player.recent_matches[0].deaths, int)
    assert isinstance(player.recent_matches[0].assists, int)
    assert isinstance(player.recent_matches[0].skill, int)
    assert isinstance(player.recent_matches[0].lane, (int, type(None)))
    assert isinstance(player.recent_matches[0].lane_role, (int, type(None)))
    assert isinstance(player.recent_matches[0].is_roaming, (bool, type(None)))
    assert isinstance(player.recent_matches[0].cluster, int)
    assert isinstance(player.recent_matches[0].leaver_status, int)
    assert isinstance(player.recent_matches[0].party_size, (int, type(None)))


def test_matches():
    player = Player(ACCOUNT_ID)
    matches = player.matches(limit=1)
    assert isinstance(matches[0], Match)
    assert isinstance(matches[0].player_slot, int)
    assert isinstance(matches[0].radiant_win, bool)
    assert isinstance(matches[0].duration, int)
    assert isinstance(matches[0].game_mode, int)
    assert isinstance(matches[0].lobby_type, int)
    assert isinstance(matches[0].hero_id, int)
    assert isinstance(matches[0].start_time, int)
    assert isinstance(matches[0].version, (int, type(None)))
    assert isinstance(matches[0].kills, int)
    assert isinstance(matches[0].deaths, int)
    assert isinstance(matches[0].assists, int)
    assert isinstance(matches[0].skill, int)
    assert isinstance(matches[0].party_size, int)


def test_win_loss():
    player = Player(ACCOUNT_ID)
    wl = player.wl(limit=10)
    assert isinstance(wl, dict)
    assert isinstance(wl.get("win"), int)
    assert isinstance(wl.get("lose"), int)


def test_totals():
    player = Player(ACCOUNT_ID)
    totals = player.totals(hero_id=69)

    for total in totals:
        assert isinstance(total, dict)
        assert isinstance(total.get("field"), str)
        assert isinstance(total.get("n"), (float, int))
        assert isinstance(total.get("sum"), (float, int))


def test_counts():
    player = Player(ACCOUNT_ID)
    counts = player.counts(hero_id=69)
    assert isinstance(counts, dict)
    assert isinstance(counts['leaver_status'], dict)
    assert isinstance(counts['game_mode'], dict)
    assert isinstance(counts['lobby_type'], dict)
    assert isinstance(counts['lane_role'], dict)
    assert isinstance(counts['region'], dict)
    assert isinstance(counts['patch'], dict)


def test_heroes():
    player = Player(ACCOUNT_ID)
    heroes = player.heroes(limit=10)
    assert isinstance(heroes[0], Hero)
    assert isinstance(heroes[0].last_played, int)
    assert isinstance(heroes[0].games, int)
    assert isinstance(heroes[0].win, int)
    assert isinstance(heroes[0].with_games, int)
    assert isinstance(heroes[0].with_win, int)
    assert isinstance(heroes[0].against_games, int)
    assert isinstance(heroes[0].against_win, int)


def test_peers():
    player = Player(ACCOUNT_ID)
    players = player.peers(limit=100)
    assert isinstance(players[0].id, int)
    assert isinstance(players[0].account_id, int)
    assert isinstance(players[0].last_played, int)
    assert isinstance(players[0].win, int)
    assert isinstance(players[0].games, int)
    assert isinstance(players[0].with_win, int)
    assert isinstance(players[0].with_games, int)
    assert isinstance(players[0].against_win, int)
    assert isinstance(players[0].against_games, int)
    assert isinstance(players[0].with_gpm_sum, int)
    assert isinstance(players[0].with_xpm_sum, int)


def test_pros():
    player = Player(86745912)
    players = player.pros(limit=1000)
    assert isinstance(players[0].id, int)
    assert isinstance(players[0].account_id, int)
    assert isinstance(players[0].against_win, int)
    assert isinstance(players[0].against_games, int)
    assert isinstance(players[0].country_code, str)
    assert isinstance(players[0].fantasy_role, int)
    assert isinstance(players[0].fh_unavailable, bool)
    assert isinstance(players[0].games, int)
    assert isinstance(players[0].is_locked, bool)
    assert isinstance(players[0].is_pro, bool)
    assert isinstance(players[0].locked_until, (int, type(None)))
    assert isinstance(players[0].last_played, int)
    assert isinstance(players[0].team_id, int)
    assert isinstance(players[0].team_name, str)
    assert isinstance(players[0].team_tag, str)
    assert isinstance(players[0].win, int)
    assert isinstance(players[0].with_win, int)
    assert isinstance(players[0].with_games, int)
    assert isinstance(players[0].with_gpm_sum, (int, type(None)))
    assert isinstance(players[0].with_xpm_sum, (int, type(None)))
