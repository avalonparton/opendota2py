""" Tests Hero functionality
https://docs.opendota.com/#tag/heroes
"""
import sys
sys.path.append(".")
sys.path.append("..")
from opendota2py import Hero, Match, Player


def check_if_antimage(hero):
    """ Asserts that the given hero is Anti-Mage """
    assert hero.id == 1
    assert hero.hero_id == 1
    assert hero.name == 'npc_dota_hero_antimage'
    assert hero.localized_name == 'Anti-Mage'
    assert hero.attack_type == 'Melee'
    assert hero.primary_attr == 'agi'
    assert hero.legs == 2

def test_hero():
    """ Test Hero functions using Anti-Mage """
    hero = Hero(1)
    check_if_antimage(hero)

    # Check the rest of the attributes
    assert isinstance(hero.info, str)
    assert isinstance(hero.roles, list)
    assert isinstance(hero.thumbnail, str)
    assert 'api.opendota.com' in hero.thumbnail
    assert isinstance(hero.thumbnail_small, str)
    assert 'api.opendota.com' in hero.thumbnail_small
    assert isinstance(hero.url, str)
    assert 'www.opendota.com' in hero.url

    # Check __str__() and load the data
    print("Done checking", hero)


def test_hero_matches():
    hero = Hero(1)
    match = hero.matches[0]
    # Check for player attributes
    assert isinstance(match, Match)
    assert isinstance(match.player_slot, int)
    assert isinstance(match.account_id, int)
    assert isinstance(match.kills, int)
    assert isinstance(match.deaths, int)
    assert isinstance(match.assists, int)
    assert isinstance(match.radiant, bool)
    assert isinstance(match.league_name, str)
    # Check that properties were set
    assert isinstance(match.match_id, int)
    assert isinstance(match.start_time, int)
    assert isinstance(match.duration, int)
    assert isinstance(match.radiant_win, bool)
    assert isinstance(match.league_id, int)
    # Check that the full match endpoint was not called
    assert match._patch == None


def test_hero_matchups():
    hero = Hero(1)
    assert isinstance(hero.matchups[0].get("hero_id"), int)
    assert isinstance(hero.matchups[0].get("games_played"), int)
    assert isinstance(hero.matchups[0].get("wins"), int)

    
def test_hero_durations():
    hero = Hero(1)
    assert isinstance(hero.durations[0].get("duration_bin"), int)
    assert isinstance(hero.matchups[0].get("games_played"), int)
    assert isinstance(hero.matchups[0].get("wins"), int)

    
def test_hero_players():
    hero = Hero(1)
    assert isinstance(hero.players[0], Player)
    assert isinstance(hero.players[0].id, int)
    assert isinstance(hero.players[0].account_id, int)
    assert isinstance(hero.players[0].games_played, int)
    assert isinstance(hero.players[0].hero_wins, int)
